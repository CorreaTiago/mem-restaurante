var Menu = (function() {

	var $container = $( '#rm-container' ),
		$cover = $container.find( 'div.rm-cover' ),
		$middle = $container.find( 'div.rm-middle' ),
		$right = $container.find( 'div.rm-right' ),
		$open = $cover.find('a.rm-button-open'),
		$close = $right.find('span.rm-close'),
		$vinho = $right.find('span.rm-vinho'),
		$details = $container.find( 'a.rm-viewdetails' ),

		init = function() {

			initEvents();

		},
		initEvents = function() {

			$open.on( 'click', function( event ) {
				openMenu();
				return false;

			} );

			$close.on( 'click', function( event ) {

				closeMenu();
				return false;

			} );

			$vinho.on( 'click', function( event ) {

				changeWine();
				return false;

			} );

			$details.on( 'click', function( event ) {

				$container.removeClass( 'rm-in' ).children( 'div.rm-modal' ).remove();
				viewDetails( $( this ) );
				return false;

			} );

		},
		openMenu = function() {

			$container.addClass( 'rm-open' );

		},
		changeWine = function() {
			if (document.getElementById("wine1").style.display == "none")
				{
					document.getElementById("wine1").style.display = "inLine"
					document.getElementById("wine2").style.display = "inLine"
					document.getElementById("wine3").style.display = "inLine"
				  document.getElementById("food1").style.display = "none"
					document.getElementById("food2").style.display = "none"
					document.getElementById("food3").style.display = "none"
			 	  document.getElementById("wineTag").innerHTML = "Principal"
				}
			else
				{
					document.getElementById("wine1").style.display = "none"
					document.getElementById("wine2").style.display = "none"
					document.getElementById("wine3").style.display = "none"
				  document.getElementById("food1").style.display = "inLine"
					document.getElementById("food2").style.display = "inLine"
					document.getElementById("food3").style.display = "inLine"
					document.getElementById("wineTag").innerHTML = "Vinhos"
				}
		},
		closeMenu = function() {

			$container.removeClass( 'rm-open rm-nodelay rm-in' );

		},
		viewDetails = function( recipe ) {

			var title = recipe.text(),
				img = recipe.data( 'thumb' ),
				description = recipe.parent().next().text(),
				url = recipe.attr( 'href' );

			var $modal = $( '<div class="rm-modal"><div class="rm-thumb" style="background-image: url(' + img + ')"></div><h5>' + title + '</h5><p>' + description + '</p><span class="rm-close-modal">x</span></div>' );

			$modal.appendTo( $container );

			var h = $modal.outerHeight( true );
			$modal.css( 'margin-top', -h / 2 );

			setTimeout( function() {

				$container.addClass( 'rm-in rm-nodelay' );

				$modal.find( 'span.rm-close-modal' ).on( 'click', function() {

					$container.removeClass( 'rm-in' );

				} );

			}, 0 );

		};

	return { init : init };

})();
